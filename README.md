Zumex, market leader in the field of automatic orange presses, offers a full range of products for pressing fresh fruit juice (an orange press in various shapes and sizes, but also a press for all types of fruit and vegetables). With Zumex you can enjoy 100% freshly squeezed juice.

This machines are made according to innovative technologies and impressive design. Zumex stands for quality. That is what your company wants to radiate too? The machines have been developed with the aim of giving an ultimate experience in addition to a fresh juice. The complete range is ideal for commercial use and has the most interesting payback time. With a Zumex you are assured of a maximum return!

This appliance is absolutely magnificent. I tried two other juicers, one automatic and one manual both of which were not even close to being good. So far I've used it with orange and lemons.

* The [Zumex](http://zumex-nederland.nl) is quiet, sturdy, and delivers citrus juice instantaneously.

* It strains fast, presses lemons and Oranges easily, and has a phenomenally sturdy base. I'm sure it does oranges and grapefruits exceptionally well also.

* The base has a notch in it so that you can fit a pretty large / tall cup under the spout.

* The spout pours the juice down as long as you release it. If you remove the cup, just lock the spout in place so residual juice doesn't drip down.

* The strainers are very good. If you use the coarse strainer, very fine pits (smaller than the pulp) will sometimes fall through. In addition, not all pulp seeps through the strainer but you can pick out what you want. The fine pits and reduced pulp are to be expected from the coarse strainer. If you only want juice to seep through, then use the fine strainer.

* Assembly is very easy and there are not too many parts. Be sure to snap the fruit dome into place. I didn't and it kept flying off during operation until I realized I didn't snap it in tightly.


* I didn't feel the press was too heavy compared to my full-size Kitchen Aid mixer. However, it is heavier than most of the cheaper citrus juicers on the market but that is to be expected from such a sturdy appliance.

* I found clean-up to be extremely easy unlike some of the other users.

It takes less pressure than flushing a toilet to operate--you can even hold the lever down with your chin if for some reason your hands were occupied or troubling you. If you press too hard, you'll strip the fruit; a light touch is all that's needed.

For me, this is a fabulous appliance, that is quick, easy, sturdy, and a pleasure to use. Operation and assembly is almost intuitive.